*** Settings ***
Library    SeleniumLibrary
Library    String

*** Variables ***
${HOME_URL_DEPRECATED}      http://automationpractice.com
${HOME_URL}                 http://www.automationpractice.pl/
${HOME_TITLE}               My Shop
${HOME_FIELD_PESQUISAR}     name=search_query
${HOME_BTN_PESQUISAR}       name=submit_search
${HOME_TOPMENU}             xpath=//*[@id="block_top_menu"]/ul
${HOME_PRODUCT}             xpath=//*[@id="center_column"]//img[@alt="Blouse"]
${PRODUCT_COLOR}            xpath=//*[@id="color_8"]
${SELECTED_COLOR}           xpath=//*[@id="color_to_pick_list"]/li[2]//*[@id="color_11"]
${HOME_BTN_ADDCART}         xpath=//*[@id="add_to_cart"]/button
${HOME_BTN_CHECKOUT}        xpath=//*[@id="layer_cart"]//a[@title="Proceed to checkout"]

*** Keywords ***
#### Ações
Adicionar o produto "${PRODUTO}" no carrinho
    Digitar o nome do produto "${PRODUTO}" no campo de pesquisa
    Clicar no botão pesquisar
    Clicar no botão "Add to Cart" do produto
    Clicar no botão "Proceed to checkout"

Acessar a página home do site
    Go To               ${HOME_URL}
    Wait Until Element Is Visible    ${HOME_TOPMENU}
    Title Should Be     ${HOME_TITLE}

Digitar o nome do produto "${PRODUTO}" no campo de pesquisa
    Input Text          ${HOME_FIELD_PESQUISAR}    ${PRODUTO}

Clicar no botão pesquisar
    Click Element       ${HOME_BTN_PESQUISAR}

Selecionar cor do produto
    Click Element                   ${PRODUCT_COLOR}
    Wait Until Element Is Visible   ${SELECTED_COLOR}
    Execute JavaScript              window.scrollTo(0,100)
    Sleep                           2s
    Wait Until Element Is Visible   ${HOME_BTN_ADDCART}

Clicar no botão "Add to Cart" do produto
    Wait Until Element Is Visible   ${HOME_PRODUCT}
    Click Element                   ${HOME_PRODUCT}
    Wait Until Element Is Visible   ${PRODUCT_COLOR}
    Wait Until Keyword Succeeds     3x  1s  Selecionar cor do produto
    Click Button                    ${HOME_BTN_ADDCART}

Clicar no botão "Proceed to checkout"
    Wait Until Element Is Visible   ${HOME_BTN_CHECKOUT}
    Click Element                   ${HOME_BTN_CHECKOUT}
